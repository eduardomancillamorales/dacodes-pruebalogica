import Foundation

public enum Direction: String {
    case right = "R"
    case left = "L"
    case up = "U"
    case down = "D"
}

open class GridManager {
    
    public init() {
        
    }
    
    private func allSquaredAreVisited(mat: [[Bool]]) -> Bool {
        for col in mat {
            for row in col {
                if row == false {
                    return false
                }
            }
        }
        return true
    }

    private func printM(_ mat: [[Bool]], dir: Direction) {
        print(">>>>>>>>>>>>>>INIT")
        for col in mat {
            print(col.map { $0 ? "X" : "0"})
        }
        switch dir {
        case .right: print("direction:    →")
        case .left: print("direction:    ←")
        case .up: print("direction:    ↑")
        case .down: print("direction:    ↓")
        }
        print(">>>>>>>>>>>>>>END")
        print("")
    }

    public func resolveMatrix(n: Int, m: Int, verbose: Bool) -> Direction {
        var matrix = self.createMatrix(n: n, m: m)
        var x = 0 //row position
        var y = 0 //col position
        var dx = 1
        var dy = 0
        var dir: Direction = .right
        
        while allSquaredAreVisited(mat: matrix) == false {
            if verbose {
                print("actual position x: \(x), y = \(y)")
            }
            
            let rowActive = matrix[y][x]
            if !rowActive {
                matrix[y][x] = true
            }
            if verbose {
                printM(matrix, dir: dir)
            }
            
            if allSquaredAreVisited(mat: matrix) { break }
            
            let tmpX = x + dx
            let tmpY = y + dy
            
            if !validateBounds(mat: matrix, x: tmpX, y: tmpY) || matrix[tmpY][tmpX] {
                //cambiar de direccion
                dir = changeDirection(dir: dir)
                switch dir {
                case .down:
                    dx = 0
                    dy = 1
                case .right:
                    dx = 1
                    dy = 0
                case .left:
                    dx = -1
                    dy = 0
                case .up:
                    dx = 0
                    dy = -1
                }
            }
            x += dx
            y += dy
        }
        return dir
    }

    private func changeDirection(dir: Direction) -> Direction {
        switch dir {
        case .down:
            return .left
        case .right:
            return .down
        case .left:
            return .up
        case .up:
            return .right
        }
    }


    private func validateBounds(mat: [[Bool]], x: Int, y: Int) -> Bool {
        if y < 0 || y >= mat.count {
            return false
        }
        
        if x < 0 || x >= mat[0].count {
            return false
        }
        return true
    }

    private func createMatrix(n: Int, m: Int) -> [[Bool]] {
        var matrix = [[Bool]]()
        for _ in 0..<m {
            let rowBool = [Bool](repeating: false, count: n)
            matrix.append(rowBool)
        }
        return matrix
    }
}
