//
//  main.swift
//  Program1
//
//  Created by eduardo mancilla on 08/12/20.
//

import Foundation

func start() {
    let manager = GridManager()
    print("Please enter T number of cases:")
    let tCases = readLineNumber(messageError: "debe ser un valor entre 1 y 5000") { value in
        return value >= 1 && value < 5000
    }
    var arrayResults: [Direction] = [Direction](repeating: .up, count: tCases)
    for pos in 0..<tCases {
        print("case number \(pos + 1) >")
        print("input N(row) value:")
        let n = readLineNumber(messageError: "debe ser mayor o igual que 1") { value in
            return value >= 1
        }
        print("input M(COLUMN) value:")
        let m = readLineNumber(messageError: "debe ser menor o igual que 10^9") { value in
            return value <= Int(pow(Double(10),Double(9))) && value > 0
        }
        arrayResults[pos] = manager.resolveMatrix(n: n, m: m, verbose: true)
    }
    for v in arrayResults {
        print("Resultado: \(v.rawValue)")
    }
}

func readLineNumber(messageError: String, closureCondition: (Int) -> Bool) -> Int {
    if let input = readLine(), let intValue = Int(input) {
        if closureCondition(intValue) {
            return intValue
        } else {
            print(messageError)
            return readLineNumber(messageError: messageError, closureCondition: closureCondition)
        }
    } else {
        print("Please enter a number")
        return readLineNumber(messageError: messageError, closureCondition: closureCondition)
    }
}

start()
